local ls = require("luasnip")
local utils = require("latex-snippets.util.utils")
local pipe = utils.pipe

local M = {}

function M.retrieve(not_math)
  local parse_snippet = ls.extend_decorator.apply(ls.parser.parse_snippet, {
    condition = pipe({ not_math }),
  }) --[[@as function]]

  return {
    parse_snippet({ trig = "mk", name = "Math" }, "\\( ${1:${TM_SELECTED_TEXT}} \\)$0"),
    parse_snippet({ trig = "dm", name = "Block Math" }, "\\[\n\t${1:${TM_SELECTED_TEXT}}\n.\\] $0"),


  parse_snippet({ trig = "medskip", name = "medskip" }, "\n \\medskip \n "),
  parse_snippet({ trig = "eqref", name = "eqref" }, "\\eqref{eq:$1} $0 "),
  parse_snippet({ trig = "ref", name = "ref" }, "Figure~\\ref{fig:$1} $0 "),
  parse_snippet({ trig = "cite", name = "cite" }, "\\cite{$1} $0 "),
  parse_snippet({ trig = "usepackage", name = "usepackage" }, "\\usepackage{$1} $0 "),
  parse_snippet({ trig = "centering", name = "centering" }, "\\centering $1 "),
  parse_snippet({ trig = "hline", name = "hline" }, "\\hline $1 "),

  }
end

return M
