local ls = require("luasnip")
local t = ls.text_node
local i = ls.insert_node

local M = {}

function M.retrieve(not_math)
  local utils = require("latex-snippets.util.utils")
  local pipe = utils.pipe

  local conds = require("luasnip.extras.expand_conditions")
  local condition = pipe({ conds.line_begin, not_math })

  local parse_snippet = ls.extend_decorator.apply(ls.parser.parse_snippet, {
    condition = condition,
  }) --[[@as function]]

  local s = ls.extend_decorator.apply(ls.snippet, {
    condition = condition,
  }) --[[@as function]]

  return {
	  -- my snippets first
	s(
		{ trig = "ALI", name = "Align" },
		{ t({ "\\begin{align}", "\t" }), i(1), t({ "", "\\label{eq:" }), i(2), t("}"), t({ "", "\\end{align}" }) }
	),

	s(
		{ trig = "EQ", name = "equation" },
		{ t({ "\\begin{equation}", "\t" }), i(1), t({ "", "\\label{eq:" }), i(2), t("}"), t({ "", "\\end{equation}" }) }
	),
	s(
		{ trig = "ITEM", name = "itemize" },
		{ t({ "\\begin{itemize}", "\t" }), t("\\item "), i(1), t({ "", "\\end{itemize}" }) }
	),

	s(
		{ trig = "FIG", name = "figure" },
		{
			t({ "\\begin{figure}[H]", "\t" }),
			t({ "\\centering", "\t" }),
			t("\\includegraphics[width=5cm]{"),
			i(1),
			t({ ".png}", "\t" }),
			t("\\caption{"),
			i(2),
			t({ "}", "\t" }),
			t("\\label{fig:"),
			i(3),
			t("}"),
			t({ "", "\\end{figure}" }),
		}
	),


	s(
		{ trig = "TAB", name = "table" },
		{
			t({ "\\begin{table}[H]", "\t" }),
			t({ "\\begin{center}", "\t" }),
			t({ "\\begin{tabular}[c]{|l|l|}", "\t" }),
			t({ "\\hline", "\t" }),
			t({ "\\multicolumn{1}{|c|}{\\textbf{}} &", "\t" }),
			t({ "\\multicolumn{1}{|c|}{\\textbf{}} \\\\", "\t" }),
			t({ "\\hline", "\t" }),
			i(1),
			t({ " & b \\\\", "\t" }),
			t({ "\\hline", "\t" }),
			t({ "a & b \\\\", "\t" }),
			t({ "\\hline", "\t" }),
			t({ "\\end{tabular}", "\t" }),
			t({ "\\end{center}", "\t" }),
			t({ "\\caption{}", "\t" }),
			t({ "\\label{tab:}", "\t" }),
			t({ "\\end{table}", "\t" }),
		}
	),


	ls.parser.parse_snippet({ trig = "BEG", name = "begin{} / end{}" }, "\\begin{$1}\n\t$0\n\\end{$1}"),
	ls.parser.parse_snippet({ trig = "CASE", name = "cases" }, "\\begin{cases}\n\t$1\n\\end{cases}"),

	s({ trig = "TEMPLATE", name = "template" }, {
		t({ "\\documentclass[12pt,a4paper]{article}" }),
		t({ "", "\\usepackage[utf8]{inputenc}" }),
		t({ "", "\\usepackage[french]{babel}" }),
		t({ "", "\\usepackage[T1]{fontenc}" }),
		t({ "", "\\usepackage{enumitem}" }),
		t({ "", "\\usepackage{amsfonts,amsmath,amssymb}" }),
		t({ "", "\\usepackage{geometry}" }),
		t({ "", "\\usepackage{empheq}" }),
		t({ "", "\\usepackage{graphicx}" }),
		t({ "", "\\usepackage{bm}" }),
		t({ "", "\\usepackage{tikz}" }),
		t({ "", "\\usepackage[paper=portrait,pagesize]{typearea}" }),
		t({ "", "\\usepackage{layout}" }),
		t({ "", "\\usepackage{float}" }),
		t({ "", "\\newcommand*\\diff{\\mathop{}\\!\\mathrm{d}}" }),
		t({ "", "\\newcommand*\\Diff[1]{\\mathop{}\\!\\mathrm{d^#1}}" }),
		t({ "", "\\newcommand{\\Ma}{\\mathrm{Ma}}" }),
		t({ "", "\\newcommand{\\Rey}{\\mathrm{Re}}" }),
		t({ "", "\\newcommand{\\cs}{c_{s}^{2}}" }),
		t({ "", "\\title{" }),
		i(1, "LBM"),
		t({ "}", }),
		t({ "", "\\author{" }),
		i(2, "Thomas Gregorczyk"),
		t({ "}" }),
		t({ "", "\\date{}" }),
		t({ "", "\\begin{document}" }),
		t({ "", "\\maketitle" }),
		t( "", "\t" ),
		i(3, "..."),
		t(""),
		t({ "", "\\begin{center}" }),
		t({ "", "\\bibliographystyle{unsrt}" }),
		t({ "", "\\bibliography{library}" }),
		t({ "", "\\end{center}" }),
		t({ "", "\\end{document}" }),
	}),

    s({ trig = "bigfun", name = "Big function" }, {
      t({ "\\begin{align*}", "\t" }),
      i(1),
      t(":"),
      t(" "),
      i(2),
      t("&\\longrightarrow "),
      i(3),
      t({ " \\", "\t" }),
      i(4),
      t("&\\longmapsto "),
      i(1),
      t("("),
      i(4),
      t(")"),
      t(" = "),
      i(0),
      t({ "", ".\\end{align*}" }),
    }),
  }
end

return M
